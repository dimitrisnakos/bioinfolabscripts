# BioinfoLabScripts

This repository contains all code/scripts I've written for the lab essays at "Bioinformatics" undergraduate course.
It includes exemplary input and output .txt files.
