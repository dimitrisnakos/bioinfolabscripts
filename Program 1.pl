#!usr/binperl -w

# Perl Essay #1 ...NAILED it:P
# Calculate distance of A(x1,y1) and B(x2,y2)
# Insert values of first point
print STDOUT "Enter point A value for x1: ";
    $x1 = <STDIN>;
    chomp($x1);
print STDOUT "Enter point A value for y1: ";
    $y1 = <STDIN>;
    chomp($y1);
say STDOUT "So the first point A has values x1=$x1 and y1=$y1.";
#insert values of second point
print STDOUT "Enter point B value for x2: ";
    $x2 = <STDIN>;
    chomp($x2);
print STDOUT "Enter point B value for y2: ";
    $y2 = <STDIN>;
    chomp($y2);
say STDOUT "So the second point B has values x2=$x2 and y2=$y2.";
#calculate the distance
    $AB = sqrt((($x2-$x1)**2)+(($y2-$y1)**2)); #mathematical equation for distance
print "The distance between point A($x1,$y1) and point B($x2,$y2) is $AB ";
getc ();
