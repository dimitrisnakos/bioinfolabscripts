#!usr/bin/perl

use warnings;

open (IN,"UniProt.txt") or die ("Fuck off"); #opens file or closes.
open (OUT,">UniProt_fasta.txt");
$/="//\n"; #$/defines where the readline ends. In his case in // followed by newline. 
while (<IN>){
	if($_=~m/^ID\s+(\w+).*\n/) { #pattern matching starts from ID and keeps as $1 the first word. and keeps on reading the rest of the file.
		print OUT ">$1\n"; #prints name with ">" in front of it, so that is made into fasta format.
	} #end if.
	while($_=~m/\n\s{5}(.*)\/\//gs) { #pattern matching start with newline and after reading 5 spaces it keeps all info (sequence in this case) as $1. 'g' causes the match or substitution to match all occurences, not just one. 's' reads text as a single string. 
		$seq.=$1; #saves the above sequence in a variable ($seq) and adds the new lines.
	}	
	$seq=~s/ //g; #~s Substitutes the first (" "in this case) with the second (nothing in this case) globally.	
	print OUT "$seq"; #prints the final results where there are no gaps or empty spaces.
	$seq='';#used for a new file output.
}
close (IN);
close (OUT);
