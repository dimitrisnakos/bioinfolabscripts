#!usr/bin/perl

use strict;
use warnings;

print "Enter mottif: ";
my $mottif=<STDIN>;
chomp $mottif;
my $motlength=length($mottif); #values that contains the lengh of the mottif.

open (IN, "UniProt_fasta.txt");

$/=">";	#Change record to operator. $/defines that the readline ends in '>'.

while(<IN>)
{
	my ($id,@proteinseq)=split(/\n/,$_); #Assigns first line to #a and the rest to the array. Splits where the regex (\n in our case) says and splits by using $_.
	
	my $seq = join('',@proteinseq); #Combines all array values into one sequence while each one has it's $a (name/ID) from which it comes.
		
	for(my $i=0;$i<(length($seq)-$motlength);$i++){ #starts a loop which reads from start till the lenght - (mottif characters), and increases by 1.
		
		my $subseq=substr($seq,$i,$motlength); #a substring taken from $seq, which starts from $i, as defined at for conditions and reads every as many characters as defined by the mottif length. Breaks down the sequence to subsequences in othee words.
					
		if ($subseq=~m/($mottif)/g){ #patter matching for mottif.
			
			print "$id\t$i\t$1\n"; #$a=ID of sequence matched, $i, Aminoacid on which it was found, $1, Mottif that was found.
		}
		else { #when no match is found 
			print "No match was found for $id\t$i.\n";
		}
	}
}
close (IN);
