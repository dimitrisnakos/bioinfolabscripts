#!usr/bin/perl -w

@genes=("gene1","gene2","gene3","gene4","gene5"); #Array made
#Move the first element to last place and last to first place only with the following commands:
#push
#pull
#shift
#unshift

$pop=pop(@genes); # last element is deleted and kept as $pop.
$shift=shift(@genes); # First element deleted and kept as $shift.
push(@genes,"$shift"); # Puts gene5 in last place.
unshift(@genes,"$pop"); # Puts gene1 in first place.
print @genes;
getc ();
